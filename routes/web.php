<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# VueJS | Routes
# Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');


# Default User route
Route::get('/', function () {return view('welcome');});
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update', 'HomeController@update')->name('update');
Route::post('/update/user', 'HomeController@update')->name('update');

// Route::get('validation', function() {});

Route::get('/gestions', function() { return view('gestion');});

# Administrateur Routes
Route::prefix('admin')->group(function() {

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/home', 'AdminController@index')->name('admin.home');

});


# Adherent Routes
Route::prefix('adherent')->group(function() {

    Route::get('/login', 'Auth\AdherentLoginController@showLoginForm')->name('adherent.login');
    Route::post('/login', 'Auth\AdherentLoginController@login')->name('adherent.login.submit');
    Route::get('/home', 'AdherentController@index')->name('adherent.dashboard');
    Route::get('/bordereau', 'FraisController@show')->name('adherent.bordereau');
    Route::get('/edit', 'AdherentController@edit')->name('edit');
    Route::get('/frais/create', 'NoteFraisController@create')->name('create');
    Route::post('/frais/create', 'FraisController@store')->name('store');
    Route::get('frais/edit', 'FraisController@edit')->name('edit.frais');
    Route::get('/frais/{frais}/delete',[
        'as' => 'frais.delete',
        'uses' => 'FraisController@destroy'
    ]);
    // Route::get('/frais/{id}', 'FraisController@show');
});

# Tresorier Routes
Route::prefix('tresorier')->group(function() {
    Route::get('/login', 'Auth\TresorierLoginController@showLoginForm')->name('tresorier.login');
    Route::post('/login', 'Auth\TresorierLoginController@login')->name('tresorier.login.submit');
    Route::get('/home', 'TresorierController@index')->name('tresorier.home');
    Route::get('/validation', 'TresorierController@validation')->name('tresorier.validation');
    Route::post('/validation', 'TresorierController@strore')->name('tresorier.validation.submit');
    Route::get('/suivi','TresorierController@suivi')->name('tresorier.suivi');
    Route::get('/validation/frais', 'TresorierController@validation')->name('tresorier.validation.frais');
});

# Application routes
Auth::routes();

# Route::get('/adherent', 'AdherentController@index')->name('adherent');
Route::get('/note', 'NoteFraisController@index')->name('note_frais');
Route::get('/user', 'UserController@index');
Route::get('/edit', 'HomeController@edit')->name('edit');


# API ROUTES
Route::get('api/motifs', 'MotifController@show');
