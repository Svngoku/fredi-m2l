<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Adherent;
use App\Tresorier;
use App\Motif;

class Frais extends Model
{
    public $table = 'frais';
    protected $fillable = [
        'id','trajet','kilometre','hebergement',
        'repas','peage','motif_id','adherent_id'
    ];

    public function adherent() {
        return $this->belongsTo(Adherent::class);
    }

    public function tresorier() {
        return $this->belongsTo(Tresorier::class);
    }

    public function motif() {
        return  $this->hasMany(Motif::class);
    }

    public function getUserFromFrais($value) {
        return $value->adherent_id ;
    }

    public function getDate() {
        return ['created_at'];
    }


}
