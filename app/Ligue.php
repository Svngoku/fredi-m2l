<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ligue extends Model
{
    public function adherent()
    {
      return $this->hasMany(Adherent::class);
    }
}
