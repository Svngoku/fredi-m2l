<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Tresorier extends Authenticatable
{
    use Notifiable;

    protected $guard = 'tresorier';
    protected $primaryKey =  'numero_tresorier';
    protected $fillable = [
       'nom', 'prenom', 'email', 'password'
    ];

    protected $hidden = ['password' , 'remeber_token'];


    public function frais() {
        
    }

    public static function euro(int $cents) {
        return number_format($cents / 100, 2, ',',' ');
    }

}
