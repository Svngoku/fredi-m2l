<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use Hamcrest\Type\IsArray;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
    /**
     * @param string | array $roles
     */
    public function authorizeRoles(Role $roles) {
       if (is_array($roles)) {
           return $this->hasAnyRole($roles) || abort(401, "Cette action n'est pas authorisé");
       }
       return $this->hasRole($roles) || abort(401, "Cette action n'est pas autorisé ");
    }

    /*
     * Verifie plusieurs roles
     * @param array $roles
     */

     public  function hasManyRoles(Role $role) {
        return null !== $this->roles()->whereIn('name', $role)->first();
     }
     /**
      * Verifie un role
      * @param string Role $role
      */

      public function hasRole(Role $role) {
          return nul !== $this->roles()->where('name', $role)->first();
      }


}
