<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemandeurFrais extends Model
{
    protected $fillable = ['name', 'email'];
}
