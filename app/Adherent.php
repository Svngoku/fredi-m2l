<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Frais;

class Adherent extends Authenticatable
{
    use Notifiable;
    protected $guard = 'adherent';

    protected $fillable = [
        'id','nom', 'prenom', 'sexe', 'adresse',
        'ville' , 'code_postale', 'telephone', 'email', 'password',
    ];

    protected $hidden = ['password', 'remeber_token'];

    // Role methode
    public function role() {
        return $this->belongsToMany(Role::class);
    }

    public function frais() {
        return $this->hasMany(Frais::class);
    }

    public function ligue()
    {
      return $this->belongsTo(Ligue::class);
    }
}
