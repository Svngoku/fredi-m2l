<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Relation entre les tables
    public function users() {
        return $this->belongsToMany(User::class);
    }

}
