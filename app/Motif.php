<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LigneFrais;
use App\Frais;

class Motif extends Model{

    protected $fillable = ['libelle'];


    public function frais() {
        return $this->belongsTo(Frais::class);
    }
    public function LigneFrais()
    {
        return $this->belongsToMany(LigneFrais::class);
    }
}
