<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Tresorier;
use App\Cerfa;
use App\Frais;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use PhpParser\Node\Expr\Cast\Double;

class TresorierController extends Controller
{
    // private $is_active = '';
    public function __construct() {
        $this->middleware('auth:tresorier');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $is_active = ' active';
        return view('pages.tresorier.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tresorier $tresorier)
    {
        // return view('pages.tresorier.frais-validation');
    }



    public function suivi(Frais $frais)
    {
        $data = [];
        // Request
        $frais = DB::select('
            SELECT email , trajet, kilometre, hebergement, repas, peage, libelle
            FROM frais
            JOIN motifs
            ON frais.motif_id = motifs.id
            JOIN adherents
            ON frais.adherent_id = adherents.id
        ');
        // Saving in $data tab
        $data[] = $frais;

        // Test fecthing values
        //dd($data);
        return View::make('pages.tresorier.suivi-frais')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
    * Validation des la note de remboursement d'un adherenbt selon l'année en cours
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function validation(Frais $frais)
    {
        $fraisInfos = [];
        $frais = DB::table('frais')
                    ->join('adherents', 'frais.adherent_id', '=' ,'adherents.id')
                    ->select('adherents.nom','frais.created_at', 'frais.adherent_id',)
                    ->get();
        $fraisInfos[] = $frais;


        // dd($frais);

        return view('pages.tresorier.frais-validation', compact('fraisInfos'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
