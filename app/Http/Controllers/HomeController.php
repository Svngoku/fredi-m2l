<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Adherent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // $request->user()->authorizeRoles(['administrateur', 'adherent']);
        return view('home');
    }

    public function timeCo() {
        if(Auth::check()) {
            $date =  date('h-m-s');
        }
        return $date;
    }

    public function edit(Adherent $adherent){
        return view('pages.edit', compact('adherent'));
    }

    public function update(Request $request, Adherent $adherent)
    {
        // $validation = new Validator();
        $adherent->name = $request->input('nom');
        $adherent->email = $request->input('email');
        $adherent->save();

        return "L'adherent  à été mis à jour ";
    }
}
