<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Motif;

class MotifController extends Controller
{
    // Constructor
    public function __construct(){
        $this->middleware('auth');
    }
    //
    public function show() {
        return DB::select('select * from motifs');
    }

    public function select_motif() {
        $motifs = DB::select('select id , libelle from motifs');
        return $motifs;
    }


}
