<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    /**
     * Constructor of AdminLoginController
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }
    /**
     * @return view
     */
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    /**
     * @param Request $user
     */

    public function login(Request $request)
    {
        // Validation du formulaire
        $this->validate($request, [
            'email' => 'required | email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // si reussis , rediriger vers la page admin
            return redirect()->intended(route('admin.home'));
        }
        // else go to login form with some data
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
