<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TresorierLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:tresorier');
    }

     /**
     * @return view
     */
    public function showLoginForm()
    {
        return view('auth.tresorier-login');
    }

    /**
     * @param Request $user
     */

    public function login(Request $request)
    {
        // Validation du formulaire
        $this->validate($request, [
            'email' => 'required | email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if(Auth::guard('tresorier')->attempt(['email' => $request->email,
            'password' => $request->password], $request->remember)) {
            // si reussis , rediriger vers la page d'acceuil du tresorier
            return redirect()->intended(route('tresorier.home'));
        }
        // else go to login form with some data
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
