<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdherentLoginController extends Controller
{
    // Constructor of Adherent
    public function __construct()
    {
        $this->middleware('guest:adherent');
    }
    /**
     * @return view
     */
    public function showLoginForm()
    {
        return view('auth.adherent-login');
    }
    // Connexion en tant qu'adherent
    public function login(Request $request)
    {
        // Validation du formulaire
        $this->validate($request, [
            'email' => 'required | email',
            'password' => 'required|min:6'
        ]);


        if(Auth::guard('adherent')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // si reussis , rediriger vers la page relative aux adherents
            return redirect()->intended(route('adherent.dashboard'));
        }
        // else redirige vers le formulaire de connexion avec des données
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
