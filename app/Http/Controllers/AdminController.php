<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    // Home page for admin
    public function index() {
        return view('pages.admin.home');
    }
    // Show an Admin with her id
    public function show($id) {

    }
    // Create a new Admin user
    public function create() {  }

    // Edit an Admin
    public function edit($id) {}
 }
