<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Adherent;

class AdherentController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth:adherent');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return view('pages.adherent.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.adherent.create');
    }

    public function edit(Adherent $adherent){
        return view('pages.edit', compact('adherent'));
    }
    /**
    * Get a validator for an incoming registration request.
    *
    * @param  array  $data
     * @param \Illuminate\Support\Facades\Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'max:100'],
            'prenom' => ['required', 'string', 'max:100'],
            'sexe' => ['required', 'string', 'max:255'],
            'licence' => ['required']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adherent = Adherent::create($request->all());
        alert()->success('Votre demande d\'adhésion a été envoyer');
        return redirect()->route('home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
