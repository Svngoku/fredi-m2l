<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoteFrais;

class RemboursementController extends Controller
{
    public function index() {
        return view('tresorier');
    }

    // State of Remboursement
    public function state(NoteFrais $frais) {
        if($frais::sum() == 7 ) {
            return $frais;
        }
    }
}
