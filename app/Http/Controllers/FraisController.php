<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\FraisRequest;
use App\Frais;
use App\Adherent;
use App\Tresorier;
use App\Http\Controllers\MotifController;

class FraisController extends Controller
{
    public function __construct(){
        $this->middleware('auth:adherent');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.note_frais');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.adherent.notesfrais.create');
    }

    public function fraisNonValide(Request $request , Frais $frais)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Frais $frais)
    {
        $data = [];
        $data['trajet'] = $request->input('trajet');
        $data['kilometre'] = $request->input('kilometre');
        $data['hebergement'] = $request->input('hebergement');
        $data['repas'] = $request->input('repas');
        $data['peage'] = $request->input('peage');
        $data['motif_id'] = $request->input('motif');
        $data['adherent_id'] = Auth::user()->id;

        if($request->isMethod('POST')) {
            // dd($data);
            $this->validate($request, [
                'trajet' => 'required',
                'kilometre' => 'required',
                'hebergement' => 'required',
                'repas' => 'required',
                'peage' => 'required',
                'motif' => 'required'
            ]);

            $frais->create($data);
        }
        // alert()->success('Votre demande de remboursement de frais a été envoyer.');
        // // $frais = Frais::create($request->all());
        alert()->success('Votre demande de remboursement de frais a été envoyer');
        return redirect()->route('adherent.bordereau', $frais->id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Frais $frais)
    {
        $userId = Auth::user()->id;
        $frais_user = Frais::all();
        // $frais[] = $userId;
        $frais = $frais_user;
        return view('pages.adherent.bordereau', compact('frais'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('pages.adherent.notesfrais.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Frais::destroy($id);
        return redirect()->route('adherent/home',alert()->success('Votre demande a été supprimer'));

    }
}
