<?php

namespace App\Http\Controllers;

use \PDF;
use Illuminate\Http\Request;
use App\NoteFrais;
use App\User;
use Dotenv\Validator;


class NoteFraisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:adherent');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/adherent/notesfrais/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, NoteFrais $note, User $user)
    {
        $note = new NoteFrais([
            'nom' => $request->get('nom'),
            'adresse' => $request->get('adresse'),
            'rue' => $request->get('rue'),
            'code_postal'=> $request->get('code_postal'),
            'motif' => $request->get('motif'),
            'montant' => $request->get('montant'),
            'date_reservation' => $request->get('date_reservation'),
            'forme_don' => $request->get('forme_don')
      ]);



      $note->save();
      return redirect('/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
