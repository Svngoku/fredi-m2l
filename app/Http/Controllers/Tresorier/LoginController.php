<?php

namespace App\Http\Controllers\Tresorier;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\HTTP\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'tresorier/home';
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $typeUser = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        foreach($this->guard()->user()->role as $role) {
            if($role->name == 'tresorier') {
                return redirect('tresorier.home');
            } elseif ($role->name == 'adherent') {
                return redirect('adherent.home');
            } else {
                return redirect('home');
            }

        }

    }

    public function guard() {
        return Auth::guard('Tresorier');
    }


}
