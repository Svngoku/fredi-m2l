<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers;
use App\User;

class UserController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    public function index() {
        $users = DB::table('users')->get();
        return view('user.index', ['users' => $users]);
    }

    public function update(Request $request, $id){
        echo 'Mise à jour de l\'utilisateur';
        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();
        return "L'utilisateur à été mis à jour ";
    }

}
