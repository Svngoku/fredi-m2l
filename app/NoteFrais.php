<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteFrais extends Model
{
    protected $fillable = [
        'nom', 'adresse', 'rue',
        'code_postal', 'objet', 'montant',
        'date_reservation', 'forme_don'
    ];
}
