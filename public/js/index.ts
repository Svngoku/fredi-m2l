// @ts-checkts
/* Function ParsingInput permettant de convertir
 *  les valeurs de types String en Number (entiers)
 */
export function parseInput(value: any) {
    return parseInt(value);
}

export function getValueTrajet(trajet: any) {
    return trajet.value;
}


