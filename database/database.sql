-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2019 at 01:59 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1-log
-- PHP Version: 7.0.33-6+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `FREDI`
--

-- --------------------------------------------------------

--
-- Table structure for table `adherents`
--

CREATE TABLE `adherents` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postale` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `adherents`
--

INSERT INTO `adherents` (`id`, `nom`, `prenom`, `sexe`, `adresse`, `ville`, `code_postale`, `telephone`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Doe', 'Jonh', 'Inconnu', '3 PLACE DU RUDEX', 'DREUX', '28100', 906907134, 'jonhdoe@m2l.fr', '$2y$10$r9v1zkj47ECNC1lHB0ZSkOF.BIIJC9Ruo7nD0ZQsTiR5OhgCXIXVy', 'QTw6O0vCmsjU0C3pow2CAzLKtgETIJwq9VMQz3eNJrcd0tduoSEmdtszn15u', '2019-04-15 23:09:39', '2019-04-15 23:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `poste`, `created_at`, `updated_at`) VALUES
(1, 'Administrateur', 'admin@m2l.com', '$2y$10$.kHxn8Gpu/1f3beTD9mMkuP/MZnL7qheEh23YEEdJbb16Yza7XmqO', 'TechLead', '2019-04-15 23:09:39', '2019-04-15 23:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `cerfa`
--

CREATE TABLE `cerfa` (
  `id` int(10) UNSIGNED NOT NULL,
  `adherent_id` int(10) UNSIGNED NOT NULL,
  `tresorier_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `demandeur_frais`
--

CREATE TABLE `demandeur_frais` (
  `id` int(10) UNSIGNED NOT NULL,
  `adherent_id` int(11) UNSIGNED NOT NULL,
  `n_reçu` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frais`
--

CREATE TABLE `frais` (
  `id` int(10) UNSIGNED NOT NULL,
  `trajet` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kilometre` int(10) UNSIGNED NOT NULL,
  `hebergement` decimal(8,2) UNSIGNED NOT NULL,
  `repas` decimal(8,2) UNSIGNED NOT NULL,
  `peage` decimal(8,2) UNSIGNED NOT NULL,
  `peage_valid` tinyint(1) DEFAULT NULL,
  `repas_valid` tinyint(1) DEFAULT NULL,
  `hebergement_valid` tinyint(1) DEFAULT NULL,
  `kilometre_valid` tinyint(1) DEFAULT NULL,
  `trajet_valid` tinyint(1) DEFAULT NULL,
  `motif_id` int(10) UNSIGNED NOT NULL,
  `adherent_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `frais`
--

INSERT INTO `frais` (`id`, `trajet`, `kilometre`, `hebergement`, `repas`, `peage`, `peage_valid`, `repas_valid`, `hebergement_valid`, `kilometre_valid`, `trajet_valid`, `motif_id`, `adherent_id`, `created_at`, `updated_at`) VALUES
(1, 'Paris-Nantes', 350, '90.00', '44.00', '23.00', NULL, NULL, NULL, NULL, NULL, 3, 1, '2019-05-01 21:44:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(89, '2014_10_12_000000_create_users_table', 1),
(90, '2014_10_12_100000_create_password_resets_table', 1),
(91, '2018_12_04_001638_create_adherents_table', 1),
(92, '2018_12_04_001655_create_frais_table', 1),
(93, '2018_12_04_001746_create_cerfas_table', 1),
(94, '2018_12_04_005646_create_motifs_table', 1),
(95, '2018_12_06_132528_create_demandeur_frais_table', 1),
(96, '2019_04_13_134252_create_roles_table', 1),
(97, '2019_04_13_134532_create_roles_user_table', 1),
(98, '2019_04_15_204934_create_tresoriers_table', 1),
(99, '2019_04_15_220648_create_admins_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `motifs`
--

CREATE TABLE `motifs` (
  `id` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `motifs`
--

INSERT INTO `motifs` (`id`, `libelle`, `created_at`, `updated_at`) VALUES
(1, 'Compétition internationale', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(2, 'Compétition nationale', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(3, 'Compétition régoniale', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(4, 'Réunion', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(5, 'Stage', '2019-04-15 23:09:39', '2019-04-15 23:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'tresorier', 'A Tresorier User', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(2, 'adherent', 'A Adherent User', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(3, 'administrateur', 'A admin User', '2019-04-15 23:09:39', '2019-04-15 23:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `roles_user`
--

CREATE TABLE `roles_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tresoriers`
--

CREATE TABLE `tresoriers` (
  `numero_tresorier` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tresoriers`
--

INSERT INTO `tresoriers` (`numero_tresorier`, `nom`, `prenom`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'DUMOUINL', 'Franck', 'dumoulin.franck@m2l.com', '$2y$10$jwZ15vhzYtJVMIp3lfZMB.5IcF6b.ukmEWPx1Luz2jBrsNc.gDhz6', 'PUv63npANtYXuLC0EimuOunyK0xecJEFdnwiwK7eg5uQHPxdjCqnPNnreAzF', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(2, 'MARIE', 'Jane', 'marie.jane@m2l.com', '$2y$10$pGqZ8jOlJd/xUBe9tldRYOpLiybP2I6DmDZg87E0V/S169fx0YpAC', 'pgYCYePiIGdZY40OvnvMdvtMlbwgBNoONKg1O7BWVWmMf0CsKAEpaDpBXmrx', '2019-04-15 23:09:39', '2019-04-15 23:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrateur', 'admin@m2l.fr', NULL, '$2y$10$R2LxaaSPE4pn2XdOSg3fSuphqaNNURJ1A8RKxfkUuZxRT9bbcWi7S', '9d8q6a7H2aI7G2HHhP1wQdW74i4ferIyJQfMM1LCQ9MCwGwFQ7C8sxsacS0S', '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(2, 'Jonh Doe', 'jonh@m2l.fr', NULL, '$2y$10$NzbwFVgkmbGKMU85Y1.xFuGUjL1PrJp0QtmgeWAuHSQBMURngUiem', NULL, '2019-04-15 23:09:39', '2019-04-15 23:09:39'),
(3, 'Dumoulin', 'dumoulin_franck@m2l.fr', NULL, '$2y$10$D4r8lF0F4w.kJpMl9cOdt.JMRwue9NgGJAiiCgg0SlSFtdjeEJ50i', NULL, '2019-04-15 23:09:39', '2019-04-15 23:09:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adherents`
--
ALTER TABLE `adherents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `adherents_email_unique` (`email`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `cerfa`
--
ALTER TABLE `cerfa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adherent_id` (`adherent_id`),
  ADD KEY `tresorier_id` (`tresorier_id`);

--
-- Indexes for table `demandeur_frais`
--
ALTER TABLE `demandeur_frais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `demandeur_frais_n_reçu_unique` (`n_reçu`),
  ADD KEY `adherent_id` (`adherent_id`);

--
-- Indexes for table `frais`
--
ALTER TABLE `frais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `motif_id` (`motif_id`),
  ADD KEY `adherent_id` (`adherent_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motifs`
--
ALTER TABLE `motifs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_user`
--
ALTER TABLE `roles_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tresoriers`
--
ALTER TABLE `tresoriers`
  ADD PRIMARY KEY (`numero_tresorier`),
  ADD UNIQUE KEY `tresoriers_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adherents`
--
ALTER TABLE `adherents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cerfa`
--
ALTER TABLE `cerfa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `demandeur_frais`
--
ALTER TABLE `demandeur_frais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frais`
--
ALTER TABLE `frais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `motifs`
--
ALTER TABLE `motifs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `roles_user`
--
ALTER TABLE `roles_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tresoriers`
--
ALTER TABLE `tresoriers`
  MODIFY `numero_tresorier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cerfa`
--
ALTER TABLE `cerfa`
  ADD CONSTRAINT `cerfa_ibfk_1` FOREIGN KEY (`adherent_id`) REFERENCES `adherents` (`id`),
  ADD CONSTRAINT `cerfa_ibfk_2` FOREIGN KEY (`tresorier_id`) REFERENCES `tresoriers` (`numero_tresorier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `demandeur_frais`
--
ALTER TABLE `demandeur_frais`
  ADD CONSTRAINT `demandeur_frais_ibfk_1` FOREIGN KEY (`adherent_id`) REFERENCES `adherents` (`id`);

--
-- Constraints for table `frais`
--
ALTER TABLE `frais`
  ADD CONSTRAINT `frais_ibfk_1` FOREIGN KEY (`motif_id`) REFERENCES `motifs` (`id`),
  ADD CONSTRAINT `frais_ibfk_2` FOREIGN KEY (`adherent_id`) REFERENCES `adherents` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
