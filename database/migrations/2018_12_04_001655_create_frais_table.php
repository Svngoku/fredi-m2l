<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trajet', 100);
            // $table->string('motifs',255);
            $table->integer('kilometre')->unsigned();
            $table->decimal('hebergement')->unsigned();
            $table->decimal('repas')->unsigned();
            $table->decimal('peage')->unsigned();
            $table->boolean('peage_valid')->nullable();
            $table->boolean('repas_valid')->nullable();
            $table->boolean('hebergement_valid')->nullable();
            $table->boolean('kilometre_valid')->nullable();
            $table->boolean('trajet_valid')->nullable();
            $table->integer('motif_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frais');
    }
}
