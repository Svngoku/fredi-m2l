<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_tresorier = new Role();
        $role_tresorier->name = 'tresorier';
        $role_tresorier->description = 'A Tresorier User';
        $role_tresorier->save();

        /*
         * 
         */
        $role_adherent = new Role();
        $role_adherent->name = 'adherent';
        $role_adherent->description = 'A Adherent User';
        $role_adherent->save();

        $role_admin = new Role();
        $role_admin->name = 'administrateur';
        $role_admin->description = 'A admin User';
        $role_admin->save();

    }
}
