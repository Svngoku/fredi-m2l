<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'administrateur')->first();
        $role_tresorier = Role::where('name', 'employee')->first();
        $role_adherent = Role::where('name', 'adherent')->first();

        // Utilisateurs ayant le role d'admin
        $admin = new User();
        $admin->name = 'Administrateur';
        $admin->email = 'admin@m2l.fr';
        $admin->password = bcrypt('m2l#19');
        $admin->save();
        $admin->roles()->attach($role_admin);

        // Utilisateurs ayant le role de Tresorier

        $tresorier = new User();
        $tresorier->name = 'Jonh Doe';
        $tresorier->email = 'jonh@m2l.fr';
        $tresorier->password = bcrypt('m2l#19');
        $tresorier->save();
        $tresorier->roles()->attach($role_tresorier);


        // Utilisateurs ayant le role d'Adherent
        $adherent = new User();
        $adherent->name = 'Dumoulin';
        $adherent->email = 'dumoulin_franck@m2l.fr';
        $adherent->password = bcrypt('m2l#19');
        $adherent->save();
        $adherent->roles()->attach($role_adherent);

    }
}
