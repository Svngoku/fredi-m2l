<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        # User seeder .
        $this->call(UserTableSeeder::class);

        # Roles seeder .
        $this->call(RoleTableSeeder::class);

        # Motifs seeder .
        $this->call(MotifsTableSeeder::class);

        # Adherent seeder.
        $this->call(AdherentTableSeeder::class);

        # Tresorier seeder
        $this->call(TresorierTableSeeder::class);

        # Administrateur seeder .
        $this->call(AdminTableSeeder::class);
    }
}
