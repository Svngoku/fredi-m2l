<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrateur = new Admin();
        $administrateur->name = 'Administrateur';
        $administrateur->email = 'admin@m2l.com';
        $administrateur->password = bcrypt('m2l#19');
        $administrateur->poste = 'TechLead';
        // Save Seeder
        $administrateur->save();
    }
}
