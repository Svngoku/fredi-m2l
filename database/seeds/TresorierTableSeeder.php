<?php

use Illuminate\Database\Seeder;
use App\Tresorier;

class TresorierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tresorier1 = new Tresorier();
        $tresorier1->nom = 'DUMOUINL';
        $tresorier1->prenom = 'Franck';
        $tresorier1->email = 'dumoulin.franck@m2l.com';
        $tresorier1->password = bcrypt('m2l#19');
        // Save Seeder
        $tresorier1->save();


        $tresorier2 = new Tresorier();
        $tresorier2->nom = 'MARIE';
        $tresorier2->prenom = 'Jane';
        $tresorier2->email = 'marie.jane@m2l.com';
        $tresorier2->password = bcrypt('m2l#19');
        // Save Seeder
        $tresorier2->save();

    }
}
