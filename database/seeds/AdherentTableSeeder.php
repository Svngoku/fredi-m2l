<?php

use Illuminate\Database\Seeder;
use App\Adherent;

class AdherentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adhrent = new Adherent();
        $adhrent->nom = 'Doe';
        $adhrent->prenom = 'Jonh';
        $adhrent->sexe = 'Inconnu';
        $adhrent->adresse = '3 PLACE DU RUDEX';
        $adhrent->ville = 'DREUX';
        $adhrent->code_postale = '28100';
        $adhrent->telephone = 06603446776;
        $adhrent->email = 'jonhdoe@m2l.fr';
        $adhrent->password = bcrypt('m2l#19');
        $adhrent->save();

    }
}
