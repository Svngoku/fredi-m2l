<?php

use Illuminate\Database\Seeder;
use App\Motif;

class MotifsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Motif Compete internationale

        $compete_internationale = new Motif();
        $compete_internationale->libelle = 'Compétition internationale';
        $compete_internationale->save();

        // Motif Compete National

        $compete_national = new Motif();
        $compete_national->libelle = 'Compétition nationale';
        $compete_national->save();

        // Motif Compete Régionale

        $compete_regoniale = new Motif();
        $compete_regoniale->libelle = 'Compétition régoniale';
        $compete_regoniale->save();

        // Motif Reunion

        $reunion = new Motif();
        $reunion->libelle = 'Réunion';
        $reunion->save();

        // Motif Stage
        $stage = new Motif();
        $stage->libelle = 'Stage';
        $stage->save();


    }
}
