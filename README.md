# FREDI | Maison des Ligues de Lorraine

> Laravel 5.7 application de gestion des frais de la maison des ligues de Lorraine

## Quick Start

``` bash 
# Install Dependencies
composer install

# Run Migrations
php artisan migrate

# Import mock for database
php artisan db:seed

# Add virtual host if using Apache

# If you get an error about an encryption key
php artisan key:generate

# Environnement file

Modifier le fichier d\'environnement afin de mimer à fond l\'environnement locale .
Il est recommandé de bien verifier la conformité de vos identifiants MYSQL , le nom de la base de données et aussi de la génération d\'une nouvlle clé ( APP_KEY dans .env )

Si vous etes sur un environnement sous WAMP  , il est impératif de suivre cet tutoriel afin de bien mener à bien l(installation de l\'application .
    https://stackoverflow.com/questions/48057738/how-to-run-laravel-project-on-localhost-using-wamp


Si vous rencontrez un problème concernant les dépendances , il est mieux de supprimer le fichier vendor , puis de faire un 
```bash composer install```. Veiller s\'assurer de posséder la bonne version de PHP et de composer . 
# Install JS Dependencies
npm install

# Watch Files

npm run watch
```

## App Info

    Afin dans le cas ou vous rencontrer un léger problème avec les migrations , il est recommandé d'executer le fichier database.sql present dans le dossier database , qui est une replication de la base de donnée de test .

    Les données relatives aux utilisateurs sont cryptés et , les données comme le mot de passe est disponible dans la documentations utilisateur du projet .

### Author

NIONGOLO CHRYS Fé-Marty
[Svngoku](https://svngoku.github.io/svngoku.io/)

### Version

1.0.0

### License

This project is licensed under the MIT License
