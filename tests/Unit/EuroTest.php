<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Tresorier;

class EuroTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testEuro()
    {
        $this->assertEquals('1,00', Tresorier::euro(100));
    }
}
