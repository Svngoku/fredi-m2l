<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Adherent;
use App\Tresorier;

class LoginFakeUser extends TestCase
{
    /**
     * Lorsque nous avons besoin d'un utilisateur authentifié,
     * nous pouvons appeler loginWithFakeUser pour le prétendre .
     *
     * @return void
     */
    public function loginWithFakeUser()
    {
            # Créons un faux utilisateur
            $user = new Adherent([
                'id' => 2,
                'nom' => 'Charles'
            ]);
            # ensuite nous devons l’authentifier.
            $this->be($user);
        }
    }
