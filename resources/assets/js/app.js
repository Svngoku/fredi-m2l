require('bootstrap');
window.Vue = require('vue');
window.VueRouter = require('vue-router');
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// import Bordereau from './components/Bordereau.vue';




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// Vue.component('hello-world', require('./components/HelloWorld.vue'));
// Vue.component('example-component', require('./components/ExampleComponent.vue'));


Vue.component('gestion-component', {
    props: ['title', 'message'],
    template: `<div class="row justify-content-center"><div class="col-md-8"><h1> {{ title }}</h1><span>{{ message }}</span></div></div>`
});


Vue.component('bordereau', {
    props: ['title'],
    template: `<div class="card-header">{{ title }}<div>`,
    mounted: function(){
        console.log('Bordereau');
    }
});

Vue.component('foo', {
    template: `<h3>Bienvenue sur l\'application de Gestion des Frais de la Maison des ligues des Lorraine </h3>`,
    mounted: function() {
      console.log('MOUNTED!');
    }
});
// Vue.component('hello-world', Gestion);

new Vue({
    el: '#app',
    show: function(){
        return {
            title: ''
        };
   },
    created: function() {
        setTimeout(function(){
          this.shouldShow = false;
      },5000);
    },
    data: {
      shouldShow: true,
      title: '',
    }
});


/* Test Component  */
// Vue.component('home-component', {
//     data: function() {
//         return  {count: 0 };
//     },
//     template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
// });

// new Vue({ el: '#home'});





