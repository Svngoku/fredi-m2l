@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @if ($message = Session::get('sucess'))
            <div class="alert alert-success">
              <p>{{ $message }}</p>
            </div>
          @endif
            <div class="card">
                <div class="card-header">📔 Infos</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Bienvenue,  {{ Auth::user()->name }} <span class="caret"></span>
                </div>
                <br>
                <div class="row-center">
                  <div class="card">
                    <ul class="list-group">
                        <li class="list-group-item"><b>Nom</b> :  {{ Auth::user()->name }}</li>
                        <li class="list-group-item"><b>Abonné le</b> :  {{ Auth::user()->created_at->format('d/m/Y') }}</li>
                        <li class="list-group-item"><b>Email</b> :  {{ Auth::user()->email }}</li>
                    </ul>
                    <div class="row"></div>
                    <div class="d-flex flex-row-reverse">
                      <a class="btn btn-primary" role="button" href="{{ url('edit/'. Auth::user()->id) }}">Modifier</a>

                    </div>
                  </div>

                {{-- <example-component></example-component> --}}


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
