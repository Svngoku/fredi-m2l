@extends('layouts.adherent')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                      <span class="text-muted">Informations</span>
                      {{-- <span class="badge badge-secondary badge-pill">3</span> --}}
                    </h4>
                    <ul class="list-group mb-3">
                      <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                        <h6 class="my-0">N°033484838{{ Auth::user()->id }}</h6>
                          <small class="text-muted">Numéro de licence</small>
                        </div>
                        {{-- <span class="text-muted">{{ Auth::user()->id }}</span> --}}
                      </li>
                      <li class="list-group-item d-flex justify-content-between">
                        <span>Total (Dons)</span>
                        <strong><span id="sum">0 €</span></strong>
                      </li>
                    </ul>

                    {{-- <form class="card p-2">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Promo code">
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-secondary">Redeem</button>
                        </div>
                      </div>
                    </form> --}}

                  </div>
                  <div class="col-md-8 order-md-1">
                      @if ($errors->any())
                      <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                      @endif
                    <h4 class="mb-3">Notes de remboursement de frais</h4>
                    <form method="POST" class="needs-validation" action="{{ route('store')}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <div class="row">
                        <div class="col-md-6 mb-3">
                          <label for="motif">Motif</label>
                          <select type="text" class="custom-select d-block w-100" id="motif" name="motif" required>
                            <option>Choix...</option>
                            <option value="1">Compétition internationale</option>
                            <option value="2">Compétition nationale</option>
                            <option value="3">Compétition régionale</option>
                            <option value="4">Réunion</option>
                            <option value="5">Stage</option>
                          </select>
                          <div class="invalid-feedback">
                            Le motif est requis
                          </div>
                        </div>

                        <div class="col-md-6 mb-3">
                          <label for="trajet">Trajet</label>
                          <select type="text" class="custom-select d-block w-100" name="trajet" id="trajet" required>
                                <option>Choix...</option>
                                <option value="Paris-Gwada">Paris-Gwada</option>
                                <option value="Marseille-Toulouse">Marseille-Toulouse</option>
                          </select>
                          <div class="invalid-feedback">
                            Le trajet est requis afin de finaliser votre demande
                          </div>
                        </div>
                      </div>

                      <div class="mb-3">
                        <label for="kilometrage">Kms parcourus</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Km</span>
                          </div>
                          <input type="number" class="form-control" id="kilometre" name="kilometre" placeholder="Kilometre" required>
                          <div class="invalid-feedback" style="width: 100%;">
                            Nbre de kilometre est requis.
                          </div>
                        </div>
                      </div>

                        <div class="mb-3">
                            <label for="peage">Couts péage</label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">€</span>
                              </div>
                              <input type="number" class="form-control" name="peage" id="peage" placeholder="peage" required>
                              <div class="invalid-feedback" style="width: 100%;">
                                Ce champs est requis.
                              </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="repas">Couts repas</label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">€</span>
                              </div>
                              <input type="number" class="form-control" name="repas" id="repas" placeholder="repas" required>
                              <div class="invalid-feedback" style="width: 100%;">
                                Ce champs est requis.
                              </div>
                            </div>
                        </div>

                        <div class="mb-3">
                                <label for="peage">Couts Hébergement</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">€</span>
                                  </div>
                                  <input type="number" class="form-control" name="hebergement" id="hebergement" placeholder="hebergement" required>
                                  <div class="invalid-feedback" style="width: 100%;">
                                    Ce champs est requis.
                                  </div>
                                </div>
                        </div>

                        <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="save-info">
                        <label class="custom-control-label" for="save-info">Garder les informations pour la prochaine fois </label>
                      </div>
                      <hr class="mb-4">
                      <button type="submit" class="btn btn-success">Valider</button>
                      <button onclick="CoutTotalLive()" class="btn btn-warning">Estimation</button>
                    </form>

    </div>
</div>
@endsection
