@extends('layouts.adherent')

@section('content')

<div class="container">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
          <a href="/" class="btn btn-default">Back to list</a>
        </div>
        <h1>Rédaction Cerfa </h1>
        <h2>{{ $frais->trajet }}</h2>

        <div>
          Posted by {{ $frais->peage }}
          {{-- <time class="timeago" datetime="{{ $frais->created_at->toIso8601String() }}"
                title="{{ $frais->updated_at->toDayDateTimeString() }}">
            {{ $frais->updated_at->diffForHumans() }} --}}
          </time>
        </div>

        <hr>

        <div>
          {!! $frais->hebergement !!}
        </div>
      </div>


@stop
