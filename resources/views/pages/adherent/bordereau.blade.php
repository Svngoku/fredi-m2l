@extends('layouts.adherent')

@section('content')
{{-- <div class="container"> --}}
    <div class="row justify-content-center">
        <div class="col-md-10">

            <div class="card">
                    <div id="bordereau">
                        <bordereau title="📓 Bordereau "><bordereau>
                    </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Descriptif des lignes de frais <span class="caret"></span>
                </div>
                <br>
                  <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    <th scope="col">Courriel</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Motif</th>
                                    <th scope="col">Trajet</th>
                                    <th scope="col">Kms parcourus</th>
                                    <th scope="col">Péages</th>
                                    <th scope="col">Repas</th>
                                    <th scope="col">Hebergement</th>
                                    <th scope="col"><b>Total</b></th>
                                    <th scope="col"> Actions</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    <div class="row">
                                            @foreach ($frais as $f )
                                            <tr>
                                                <td>{{ Auth::user()->email }}</td>
                                                <td>{{ $f->created_at->toDayDateTimeString()  }}</td>
                                                <td>{{  $f->motif_id  }}</td>
                                                <td>{{  $f->trajet  }}</td>
                                                <td>{{  $f->kilometre  }}</td>
                                                <td>{{  $f->peage  }}</td>
                                                <td>{{ $f->repas }}</td>
                                                <td>{{ $f->hebergement }}</td>
                                                <td>{{  $f->peage + $f->repas + $f->hebergement }}</td>
                                                <td class="input-group-btn">
                                                        <a href="#" class="btn btn-primary">Edit</a>
                                                        <a href="{{ route('frais.delete', $f->id) }}" class="btn btn-danger"
                                                        onclick="return confirm('Etes vous sure de vouloir supprimé cette demande ?');">
                                                        Delete
                                                        </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                    </div>
                                <tr>
                                    <td colspan="2">Montant total des frais de deplacement  </td>
                                </tr>
                            </tbody>
                        </table>
                  </div>
            </div>
        </div>

    </div>
</div>
@endsection
