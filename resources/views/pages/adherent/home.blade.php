@extends('layouts.adherent')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">📔 Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Bienvenue sur FREDI , cher Adherent
                </div>
                <br>
                <div class="row-center">
                  <div class="card">
                    <ul class="list-group">
                        <li class="list-group-item"><b>Nom</b> :  {{ Auth::user()->nom }}</li>
                        <li class="list-group-item"><b>Prénom</b> :  {{ Auth::user()->prenom }}</li>
                        <li class="list-group-item"><b>Abonné le</b> :  {{ Auth::user()->created_at->format('d/m/Y') }}</li>
                        <li class="list-group-item"><b>Sexe</b> :  {{ Auth::user()->sexe }}</li>
                        <li class="list-group-item"><b>Adresse</b> :  {{ Auth::user()->adresse }}</li>
                        <li class="list-group-item"><b>Ville</b> :  {{ Auth::user()->ville }}</li>
                        <li class="list-group-item"><b>CP</b> :  {{ Auth::user()->code_postale }}</li>
                        <li class="list-group-item"><b>Téléphone</b> :  {{ Auth::user()->telephone }}</li>
                    </ul>
                    <div class="row"></div>
                    <div class="d-flex flex-row-reverse">
                      <a class="btn btn-primary" role="button" href="{{ url('edit/'. Auth::user()->id) }}">Modifier</a>

                    </div>
                  </div>

            </div>
        </div>
    </div>
</div>
@endsection
