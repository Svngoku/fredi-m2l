@extends('layouts.tresorier')
@section('content')
  <form class="" action="{{ url('submitForm') }}" method="post">
    {{ csrf_field() }}
    <div class="form-froup">
      <label for="nom" class="control-label"> Nom</label>
      <input type="text" class="form-control" id="name_id" name="name" placeholder="Jonh Doe">
    </div>



    <div class="form-group"> <!-- Submit Button -->
      <button type="submit" class="btn btn-primary">Soumettre</button>
    </div>
  </form>
@endsection
