@extends('layouts.tresorier')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">Suivi des payements de remboursements de frais {{ date('Y') }}</div>

                <div class="table table-responsive">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">Courriel</th>
                            <th scope="col">Date</th>
                            <th scope="col">Motif</th>
                            <th scope="col">Trajet</th>
                            <th scope="col">Kms parcourus</th>
                            <th scope="col">Péages</th>
                            <th scope="col">Repas</th>
                            <th scope="col">Hebergement</th>
                            <th scope="col"><b>Total</b></th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as list($value))
                                <tr>
                                   <td>{{ $value->email }}</td>
                                    <td>{{ date('D-m-Y') }}</td>
                                    <td>{{ $value->libelle }}</td>
                                    <td>{{ $value->trajet }}</td>
                                    <td>{{ $value->kilometre }}</td>
                                    <td>{{ $value->peage }}</td>
                                    <td>{{ $value->repas }}</td>
                                    <td>{{ $value->hebergement }}</td>
                                    <td></td>
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="2">Montant total des frais de deplacement non validé  </td>
                            </tr>
                        </tbody>
                    </table>
              </div>
            </div>

        </div>
    </div>
</div>
@endsection
