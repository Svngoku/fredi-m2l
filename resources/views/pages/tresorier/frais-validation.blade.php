@extends('layouts.tresorier')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Validation des notes de frais des bénévoles année {{ date('Y') }}</div>
                        <form method="POST"  action="{{-- route('validation-frais') --}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="col-md-6 mb-3">
                                <label for="motif">Chosir le Demandeur</label>
                                <select type="text" class="custom-select d-block w-100" id="demandeur" name="demandeur" required>
                                @foreach ($fraisInfos as list($frais) )
                                  <option>Choix...</option>
                                  <option value="{{ $frais->adherent_id }}"> {{ $frais->nom }}</option>
                                @endforeach
                                </select>
                                <div class="invalid-feedback">
                                 Le Demandeur est requis .
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="motif">Année</label>
                                <select type="text" class="custom-select d-block w-100" id="year" name="year">
                                    <option>{{ date('Y') }}<option>
                                </select>
                                <div class="invalid-feedback">
                                 L'année est requise .
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <button type="submit" class="btn btn-success">Valider</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
