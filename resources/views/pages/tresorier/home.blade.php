@extends('layouts.tresorier')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenue sur FREDI , cher Tresorier</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   Vous etes Connecté {{ Auth::user()->nom }}
                </div>
                <div class="row-center">
                    <div class="card">
                      <ul class="list-group">
                          <li class="list-group-item"><b>Nom</b> :  {{ Auth::user()->nom }}</li>
                          <li class="list-group-item"><b>Prénom</b> :  {{ Auth::user()->prenom }}</li>
                          <li class="list-group-item"><b>Abonné le</b> :  {{ Auth::user()->created_at->format('d/m/Y') }}</li>
                          <li class="list-group-item"><b>N° de Licence</b> :  3367652517{{ Auth::user()->id }}</li>


                      </ul>
                    </div>

              </div>
            </div>
        </div>
    </div>
</div>
@endsection
