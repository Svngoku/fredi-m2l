@extends('layouts.tresorier')

@section('content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="pull-left">
                <a class="btn btn-primary" href="{{ route('home') }}"> Back</a>
            </div>
            <br>
            <br>
            @if ($errors->any())
              <div class="alert alert-danger">
                <strong>Whoops ! </strong> Il y avait quelques problèmes au niveau de la saisie des champs  . <br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
              <div class="card">
                  <div class="card-header"> 📔 Informations personelles </div>
                  <br>
                  <div class="row-center">
                    <div class="card">
                      <form class="container" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="name"><b>Nom</b></label>
                          <input type="text" value="{{ Auth::user()->nome }}" class="form-control" id="name" required placeholder="Change name">
                       </div>
                        <div class="form-group">
                          <label for="email"><b>Email</b></label>
                          <input type="email" value="{{ Auth::user()->email }}" required class="form-control" id="email" placeholder="Change email">
                        </div>
                        {{-- <div class="form-group">
                                <label for="password"><b>Mot de passe</b></label>
                                <input type="password" value="{{ Auth::user()->password }}" required class="form-control" id="password" placeholder="">
                        </div> --}}
                      </form>
                      <div class="row"></div>
                      <div class="d-flex flex-row-reverse">
                          <a type="submit" href="{{ route('update') }}"  class="p-2 btn-sm btn-primary" name="valid">Valider</a>
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
