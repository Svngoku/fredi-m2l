@extends('layouts.app')
@section('content')
    <div class="container" id="gestion">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Utilisateurs </h1>
                @foreach ($users as $user )
                    <h4>{{ $user->name }}</h4>
                @endforeach
            </div>
        </div>
    </div>
@endsection
